import argparse
import requests
from loguru import logger
import sys
import json 


logger.remove()
logger.add("PyCowHelper_{time}.log",rotation="w6",format="{time:DD/MM/YYYY HH:mm:ss} - {level} - {message}")
logger.add(sys.stdout, colorize=True, format="<level>{time:DD/MM/YYYY HH:mm:ss} - {level} - {message}</level>")


def massSyncjobEdit(options):
    jobs = requests.get(f"http://{options['server']}/api/v1/get/syncjobs/all/no_log", headers={'X-API-Key':options['api_key']})
    
    if jobs.status_code != 200:
        logger.error(f"{options['action']} - Error obteniendo los syncjobs - Response text: {jobs.text}")
        return
    
    for job in jobs.json():
        if options['domain'] != None and job['user2'].split("@")[1] != options['domain']:
            logger.info(f"Salteando syncjob[{job['id']}] para cuenta [{job['user2']}] correspondiente correspondiente a otro dominio")
            continue

        if not options['target_new_jobs'] and job['last_run'] == None:
            logger.info(f"Salteando syncjob[{job['id']}] nuevo")
            continue

        if not options['target_disabled_jobs'] and job['active'] == 0:
            logger.info(f"Salteando syncjob[{job['id']}] desactivado")
            continue

        res = requests.post(f"https://{options['server']}/api/v1/edit/syncjob", headers={'X-API-Key':options['api_key']}, json=
            {
                "attr": {
                    "active": job['active'] if options['activation_status'] == None else options['activation_status'],
                    "automap": job['automap'],
                    "custom_params": job['custom_params'],
                    "enc1": job['enc1'],
                    "exclude": job['exclude'] if options['exclude'] == None else options['exclude'],
                    "host1": job['host1'] if options['remote_host'] == None else options['remote_host'],
                    "maxage": job['maxage'] if options['max_age'] == None else options['max_age'],
                    "maxbytespersecond": job['maxbytespersecond'] if options['max_bytes_per_second'] == None else options['max_bytes_per_second'],
                    "mins_interval": job['mins_interval'] if options['mins_interval'] == None else options['mins_interval'],
                    "port1": job['port1'] if options['remote_port'] == None else options['remote_port'],
                    "skipcrossduplicates": job['skipcrossduplicates'],
                    "subfolder2": job['subfolder2'] if options['subfolder'] == None else options['subfolder'],
                    "subscribeall": job['subscribeall'],
                    "timeout1": job['timeout1'] if options['remote_timeout'] == None else options['remote_timeout'],
                    "timeout2": job['timeout2'] if options['local_timeout'] == None else options['remote_timeout'],
                },
                "items": job['id']
            }
        )

        if res.status_code != 200:
            logger.error(f"{options['action']} - Error al modificar el syncjob[{job['id']}] - Response text: {res.text}")
        else:
            logger.success(f"{options['action']} - Syncjob[{job['id']}] para cuenta [{job['user2']}] actualizado exitosamente")


def checkMailbexesFullness(options):
    if options['domain'] == None:
        options['domain'] = input("Es necesario especificar un dominio para realizar esta acción.\nDominio:")

    mailboxes = requests.get(f"http://{options['server']}/api/v1/get/mailbox/all/{options['domain']}", headers={'X-API-Key':options['api_key']})

    if mailboxes.status_code != 200:
        logger.error(f"{options['action']} - Error obteniendo las casillas de email - Response text: {mailboxes.text}")
        return

    for mailbox in mailboxes.json():
        if mailbox['percent_class'] == "success" and mailbox['percent_in_use'] >= options['notification_threshold']:
            logger.warning(f"{options['action']} - La casilla de email [{mailbox['username']}] está cerca de quedarse sin espacio. Usado: {mailbox['percent_in_use']}% [Notificaciones desde: {options['notification_threshold']}%]")

            if not options['disable_external_notifications']:
                from notifier import sendEmailQuotaNotification

                sendEmailQuotaNotification(mailbox, options['notification_threshold'])


availableFunctions = {
    'massSyncJobEdit': massSyncjobEdit,
    'checkMailboxesFullness': checkMailbexesFullness
}

def main(argv):
    if argv["api_key"] is None:
        argv["api_key"] = input("MailCow API KEY: ")
    if argv["server"] is None:
        argv["server"] = input("MailCow server (IP:PORT or FQDN): ")

    availableFunctions[argv['action']](argv)

if __name__ == '__main__':
    parser = argparse.ArgumentParser(description="Python helper for MailCow automations")

    availableOptionsHelp = '''
        massSyncJobEdit => Massively edits the targeted syncjobs on a mailcow server.
        checkMailboxesFullness => Verify the fullness state of each targeted mailbox and (optionally) send a fullness notification. 
    '''
    #Basic parameters
    parser.add_argument('-k','--api-key', help="API Key for the targeted MailCow instance.")
    parser.add_argument('-s','--server', help="IP:Port of FQDN of targeted MailCow.")
    parser.add_argument('-a','--action', required=True, help=f"Task selector:\n {availableOptionsHelp}")
    parser.add_argument('-d','--domain', help="Targeted domain within the MailCow server.")

    #MassSyncJobEdit arguments
    parser.add_argument('--remote-host', help="Remote host used for syncing the targeted accounts.")
    parser.add_argument('--remote-port', help="Remote port used for syncing the targeted accounts.")
    parser.add_argument('--max-age', help="Max age allowed for the synced emails.")
    parser.add_argument('--max-bytes-per-second', help="Max speed allowed for syncjob.")
    parser.add_argument('--mins-interval', help="Minimum time (in minutes) between executions of the same syncjob.")
    parser.add_argument('--exclude', help="Exclude configuration for syncing the accounts.")
    parser.add_argument('--subfolder', help="Destination subfolder configuration for syncing the accounts.")
    parser.add_argument('--activation-status', help="Set all targeted syncjobs active/disabled.")
    parser.add_argument('--target-new-jobs', action='store_true', help="Ignores the default restriction for updating SyncJobs of only modifying previously ran jobs.")
    parser.add_argument('--target-disabled-jobs', action='store_true', help="Ignores the default restriction for updating SyncJobs of only active jobs.")
    parser.add_argument('--remote-timeout', help="Timeout for the remote server.")
    parser.add_argument('--local-timeout', help="Timeout for the local server.")

    #CheckMailboxesFullnes argumens
    parser.add_argument('--notification-threshold', type=int, default=75 ,help="Fullness level percentage after wich notifications are triggered.")
    parser.add_argument('--disable-external-notifications', action='store_true', help="Fullness level after wich external notifications may be triggered.")



    main(vars(parser.parse_args())) 