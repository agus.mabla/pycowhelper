# PyCowHelper



## Descripción

Este .py contiene automatizaciones útiles creadas para facilitar la migración de los mails a [Mailcow](https://mailcow.email/)

## Acciones disponibles

### `checkMailboxesFullness`
Verifica cuán llenas están las casillas de email de un determinado dominio. En caso de superar un límite preestablecido genera una notificación de advertencia. 

Parámetros propios

|Nombre |Descripción|
|-|-|
|`--notification-threshold` | Establece el límite (en %) a partir del cuál se emitirá una notificación advirtiendo sobre el uso de dicha cuenta.|
|`--disable-external-notifications`| Desactiva el llamado a `sendEmailQuotaNotification` evitando así las notificaciones por fuera de los logs de la aplicación.|

>Nota: En caso de no estar presente el parámetro `--disable-external-notifications` esta acción requiere la implementación de la función `sendEmailQuotaNotification` personalizada con sus propios requerimientos para el proceso de notificación. [Ver arvhico de ejemplo](./notifier.py.example)


### `massSyncJobEdit`
Edita de forma masiva los trabajos de sincronización presentes en el servidor. 

Argumentos propios:

|Nombre | Descripción|
|-|-|
|`--remote-host`|De estar presente el script actualizará el campo `Host` de todos los trabajos de sincronización alcanzados por los filtros al valor recibido por este parámetro|
|`--remote-port`|De estar presente el script actualizará el campo `Puerto` de todos los trabajos de sincronización alcanzados por los filtros al valor recibido por este parámetro|
|`--max-age`|De estar presente el script actualizará la antigüedad máxima (en días) de los emails sincronizados al valor recibido por el parámetro|
|`--max-bytes-per-second`|De estar presente el script limitará la velocidad de descarga de todos los trabajos de sincronziación alcanzados por los filtros al valor recibido por este parámetro|
|`--mins-interval`|De estar presente el script actualizará el tiempo mínimo (en minutos) de los trabajos de sincronización alcanzados por los filtros al valor recibido por este parámetro|
|`--exclude`|De estar presente el script actualizará el parámetro `exclude` de todos los trabajos de sincronización alcanzados por los filtros al valor recibido por este parámetro|
|`--subfolder`|De estar presente el script actualizará la subcarpeta de destino de los emails sincronizados en todos los trabajos de sincronización alcanzados por los filtros al valor recibido por este parámetro|
|`--activation-status`|De estar presente activa/desactiva todos los trabajos de sincronización alcanzados por los filtros. [0=>Desactivado, 1=>Activado]|
|`--remote-timeout`|De estar presente actualiza el timeout para el servidor remoto de todos los trabajos de sincronización alcanzados por los filtros al valor recibido por este parámetro|
|`--local-timeout`|De estar presente actualiza el timeout para el servidor local de todos los trabajos de sincronización alcanzados por los filtros al valor recibido por este parámetro|
|`--target-new-jobs`|Filtro: De estar presente los trabajos de sincronización modificados incluirán aquellos que nunca fueron ejecutados|
|`--target-disabled-jobs`|Filtro: De estar presente los trabajos de sincronización modificados incluirán aquellos que se encuentran desactivados|

>Nota: Todos estos parámetros son opcionales pero, de no recibir alguno, los trabajos de sincronización permanecerán en su estado previo a la ejecución de este script. 

## Parámetros generales
|Nombre | Forma corta | Descripción| Requerido
|-|-|-|-|
|`--api-key`|`-k`|[API KEY](https://github.com/mailcow/mailcow-apiblueprint) del servidor de Mailcow sobre el que se desea operar |✅|
|`--server`|`-s`|`IP:Puerto` ó `FQDN` del servidor de Mailcow sobre el que se desea operar|✅|
|`--action`|`-a`| [Acción](#acciones-disponibles) que se desea ejecutar|✅|
|`--domain`|`-d`| Dominio sobre el cuál se desean realizar las acciones. En caso de no estar presente se ejecutarán sobre todos los dominios a los que tenga acceso la [API KEY](https://github.com/mailcow/mailcow-apiblueprint)|❌|

---

## Desctiption

This script contains usefull automations made to ease email migrations to a [Mailcow](https://mailcow.email/) server.

## Available Actions

### `checkMailboxesFullness` 

It checks the fullness level of the mailboxes on a domain. In case of reaching a predetermined threshold it will trigger a warning notification. 

Custom parameters

|Name |Description|
|-|-|
|`--notification-threshold` |Stablishes the fullness percentage threshold after wich notifications are triggered.|
|`--disable-external-notifications`| Disables the callback to the external `sendEmailQuotaNotification` thus avoiding notifications outside the scrip loggin|

>Note: In case of not using the `--disable-external-notifications` switch this action requires the implementation of a customized function named `sendEmailQuotaNotification` for sending the external notifications. It should be placed within a `notifier.py` file. [Check the example file](./notifier.py.example)


### `massSyncJobEdit`
Massively edits the syncjobs present on the server. 

Custom arguments:

|Name |Description|
|-|-|
|`--remote-host`|If present the script will update the `host` field of all targeted jobs to the value provided by this parameter|
|`--remote-port`|If present the script will update the `port` field of all targeted jobs to the value provided by this parameter| 
|`--max-age`|If present the script will update the max age (in days) of the emails fetched by the syncjobs targeted to the value provied by this parameter|
|`--max-bytes-per-second`|If present the script will limit the download speed of all tergeted syncjobs to the value provided by this parameter|
|`--mins-interval`|If present the script will update the minimum time (in minutes) between executions of the same job to the value provided by this parameter. It will only update the syncjobs targeted by the script|
|`--exclude`|If present the script will update the `exclude` field on every syncjob targeted by this script to the value provided by this parameter|
|`--subfolder`|If present the script will update the destination subfolder for the targeted jobs to the value provided by this parameter|
|`--activation-status`|If present the script will activate/deactivate all the syncjobs targeted by the sctipt. [0=>Deactivate, 1=>Activate]|
|`--remote-timeout`|If present the script will update the timeout on the remote server for all the targeted jobs to the value provided by this parameter|
|`--local-timeout`|If present the script will update the timeout on the local server for all the targeted jobs to the value provided by this parameter|
|`--target-new-jobs`|Filter: If present the targeted syncjobs will include those which have never been executed|
|`--target-disabled-jobs`|Filter: If present the targeted syncjobs will include disabled ones|

>Note: All of this parameters are optionals but if none are present all the script will reman as they were before the script execution. 

## General parameters

|Name|Short form|Description|Required
|-|-|-|-|
|`--api-key`|`-k`|[API KEY](https://github.com/mailcow/mailcow-apiblueprint) of the destination Mailcow server|✅|
|`--server`|`-s`|`IP:Port` or `FQDN` of the destination Mailcos server|✅|
|`--action`|`-a`| [Action](#acciones-disponibles) to execute|✅|
|`--domain`|`-d`| Domain on which to perform the action. If this parameter is not present the actions will be executed on all domains reacheable by the [API KEY](https://github.com/mailcow/mailcow-apiblueprint)|❌|